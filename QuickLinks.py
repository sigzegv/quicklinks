import webbrowser
import subprocess
import sublime
import sublime_plugin


##
# Open link via list control
class OpenLinkCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.settings = sublime.load_settings('QuickLinks.sublime-settings')

        selection = self.view.substr(self.view.sel()[0])

        self.list = []

        for engine in self.settings.get('quicklinks'):
            url = engine[1]

            if '%s' in url :
                if selection:
                    self.list.append([engine[0], engine[1] % selection])
            else:
                self.list.append([engine[0], engine[1]])

        self.view.window().show_quick_panel(self.list, self.on_done, 1, 2)

    # start browser on selected engine
    def on_done(self, index):
        if index == -1:
            return

        openLink(self.list[index][1])


##
# Opens default link
class OpenLinkDefaultCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        settings = getSettings()

        selection = self.view.substr(self.view.sel()[0])

        url = settings.get('quicklinks')[settings.get('quicklinks_default')][1]

        if '%s' in url and not selection:
            sublime.error_message("Editor must have a selection to open default link [%s]" % url)
            return

        openLink(url%selection if "%s" in url else url)


##
# Open link in browser
def openLink(url):
    settings = getSettings()

    if settings.get('browser_command'):
        try:
            command = []
            command = settings.get('browser_command')
            command.append(url)
            subprocess.Popen(command)
            return
        except IndexError:
            print("Browser command not found on system. Using default fallback.")

    webbrowser.open(url)


##
# Get plugin settings
def getSettings():
    return sublime.load_settings('QuickLinks.sublime-settings')
