# QuickLinks for Sublime Text 2/3

## What is this plugin

QuickLinks is a plugin that will allow you to open custom sites on your browser from sublime list control.

Features :

* Create an URL list, popup it in the built-in sublime's list panel.
* Open links directly in your web browser by selecting them in the list
* Create search links managing your selected text
* Set a link as default to open it without using list panel.

## Installation

### From Package Control

Open Package Control from *Preferences => Package control* and run *Package Install*. Search for ``QuickLinks`` and click it to install.

### Manual installation

Download or clone this repository on your system in a directory called *QuickLinks*, in the Sublime Text Packages directory for your platform

``git clone https://bitbucket.org/sigzegv/quicklinks.git QuickLinks``

## Usage

Search for ``Quick Links`` in the command panel to find commands. Use ``Quick Link: List`` to pop the list panel and ``Quick Link: Default`` to open default link.

To edit settings, browse *Preferences => Package Settings => Quick Links* and open **Settings - Default** and copy its content to **Settings - User** file.
If you edit default settings directly, you changes may be overwritten by next update.

In the settings file, add any URL in the *quicklinks* key. Each entry is an array like this : ``['name', 'url']``.
To have a link managing your currently selected text, insert **%s** widlcard in your url at desired location.

If you pop the list panel with no text selected, wildcarded URLs will not be shown in the list.


Default key bindings :

* ctrl+shift+x (Windows/Linux)

    Open the list panel with you links

* ctrl+shift+w (windows/linux)

    Open your default link directly in your web browser.

## Bugs and Features Request

Please use the issue manager at https://bitbucket.org/sigzegv/quicklinks/issues/new to report bugs or request a new feature.